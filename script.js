// alert, confirm, prompt

// Типы данных
// Переменные

// console.log(`привет`)

// Тип данных - категория информации(продукта):
// 1. Примитивные и Ссылочные
// Number (числа): 1, 20, 20.5, 0.5, NaN (Not A Number)
// String (строки): 'text', "asd", `Alex`
// Boolean (логический): true, false
// Undefined: undefined
// null

// console.log(5 + 10)
// console.log(5 - 10)
// console.log(5 * 10)
// console.log(5 / 10)

// console.log(20 / 3)
// console.log(20 % 3)
// % - остаток от деления

// typeof - проверяет тип данных значения

// console.log(typeof 123)

// конкатенация - соединение строк 

// console.log('Alex' + 'Dikin')

// преобразование типов - 
// когда между значениями мы пишем все кроме + и есть строка, вычисляются как числа. Если ставится + то происходит конкатенация

// console.log('50'+5000)

// console.log('50' * 5)

// console.log('asdasd' - 20)

// isNaN - проверка на NaN, если true, то значение равно NaN,если false - то значение обычное число

// console.log(isNaN(123))

// isFinite

// console.log(1/0)

// console.log('Сумма двух чисел', 20+5)

// Переменная - ячейка памяти
// var, let, const

// var name;   // создается, инициализируется

// name = 'Ivan' // присвоение, 

// var name = 'Andre'; // создание и присвоение

// console.log(name)

// 1. var - создается глобально
// 2. перезапись уже существующей переменной

// var config = 'PASDASOIOH*@(C780rtn238';

// var config = 'fast';

// console.log(config)

// let, const

// let name = 'test'
// name = 'asd'

// console.log(name)

// const name = 'test'
// name = 'asd'

// console.log(name)

// Int(integer) - целое 
// Float - вещественные числа (с точкой), десятичные

// parseInt/parseFloat - достает из строки числа

// console.log(parseFloat('20.5px'))

const discount = '10%';
const price = 5000;

// snake_case - каждое новое слово разделяется нижним подчеркиванием  

// camelCase - каждое новое слово пишется с большой буквы

const discountPrice = (price * parseInt(discount))/100;

const finally_price = price - discountPrice;

console.log('Цена с учетом скидки:', finally_price)


const number = 79;

// const ten = parseInt(number / 10);

const ten = Math.floor(number / 10);
const ed = number % 10;

const res = ten + ed * 10;
console.log(res) 

// Math
// Math.round() - округляет по математическим правилам
// Math.ceil() - округляет в большую сторону
// Math.floor() - округляет в меньшую сторону 

// console.log(Math.floor(5.8))

